export default {
    app: {
        token: "9d686a47b1de48bab431e94750d1cd87", // <- enter your token here
        muted: false, // <- mute microphone by default
        googleIt: true // <- ask users to google their request, in case of input.unknown action
    },
    locale: {
        strings: {
            welcomeTitle: "Hola, escribinos para comenzar a hablar",
            welcomeDescription: `Podés escribir hola, o presionar el microfono para empezar a hablar`,
            offlineTitle: "Oh, no!",
            offlineDescription: "It looks like you are not connected to the internet, this webpage requires internet connection, to process your requests",
            queryTitle: "Empieza a escribir",
            voiceTitle: "Dale te estoy escuchando..."
        },
        settings: {
            speechLang: "es-AR", // <- output language
            recognitionLang: "es-ES" // <- input(recognition) language
        }
    }
}
